extends "res://addons/gut/test.gd"

var scene: PackedScene = preload("res://scenes/control/ControlDigit.tscn");
var control_digit: ControlDigit;

func before_each() -> void:
	control_digit = scene.instance() as ControlDigit;

func test_set_value() -> void:
	control_digit.set_value(5);
	assert_eq(control_digit.text, "5");

func test_get_value_default() -> void:
	assert_eq(control_digit.get_value(), 0);

func test_get_value() -> void:
	control_digit.set_value(4);
	assert_eq(control_digit.get_value(), 4);

func test_emit_touch_signal() -> void:
	watch_signals(control_digit);
	control_digit.emit_touch_signal();
	assert_signal_emitted(control_digit, "selected_control_digit");