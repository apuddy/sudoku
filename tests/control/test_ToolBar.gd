extends "res://addons/gut/test.gd"

var scene: PackedScene = preload("res://scenes/control/ToolBar.tscn");
var bar: ToolBar;

func before_each() -> void:
	bar = scene.instance() as ToolBar;
	add_child(bar);

func after_each() -> void:
	remove_child(bar);

func test_connected_to_child_signals() -> void:
	var buttons: Array = bar.get_tool_buttons();
	for button in buttons:
		assert_true(
			button.is_connected("on_toolmode_button_click", bar, "_on_toolmode_button_click"),
			"Connected to child's signal");

func test_get_tool_buttons() -> void:
	var buttons: Array = bar.get_tool_buttons();
	assert_eq(buttons.size(), 4);
	assert_is(buttons[0], ToolModeButton);