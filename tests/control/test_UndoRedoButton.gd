extends "res://addons/gut/test.gd"

var scene: PackedScene = preload("res://scenes/control/UndoButton.tscn");
var button: UndoRedoButton;

func before_each() -> void:
	button = scene.instance() as UndoRedoButton;

func test_emit_touch_signal() -> void:
	watch_signals(button);
	button.emit_touch_signal();
	assert_signal_emitted(button, "on_undo_redo_button_click");