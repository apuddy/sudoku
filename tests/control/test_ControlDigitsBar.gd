extends "res://addons/gut/test.gd"

var scene: PackedScene = preload("res://scenes/control/ControlDigitsBar.tscn");
var control_digits_bar: ControlDigitsBar;

func before_each() -> void:
	control_digits_bar = scene.instance() as ControlDigitsBar;
	add_child(control_digits_bar);

func after_each() -> void:
	remove_child(control_digits_bar);

func test_connected_to_child_signals() -> void:
	var buttons: Array = control_digits_bar.get_digit_buttons();
	for button in buttons:
		assert_true(
			button.is_connected("selected_control_digit", control_digits_bar, "_selected_control_digit"),
			"Connected to child's signal");

func test_get_digit_buttons() -> void:
	var buttons: Array = control_digits_bar.get_digit_buttons();
	assert_eq(buttons.size(), 9);
	assert_is(buttons[0], ControlDigit);

func test_selected_control_digit_emits_signal() -> void:
	watch_signals(control_digits_bar);
	control_digits_bar._selected_control_digit(4);
	assert_signal_emitted_with_parameters(control_digits_bar, "selected_digit", [4]);

func test_selected_control_digit_changes_value() -> void:
	control_digits_bar._selected_control_digit(5);

	assert_eq(control_digits_bar.current_digit, 5);

func test_selected_control_digit_deselects() -> void:
	control_digits_bar._selected_control_digit(5);
	assert_eq(control_digits_bar.current_digit, 5);
	control_digits_bar._selected_control_digit(5);
	assert_eq(control_digits_bar.current_digit, -1);

func test_deselect_other_digits() -> void:
	control_digits_bar._selected_control_digit(5);

	for child in control_digits_bar.get_digit_buttons():
		if child.get_value() == 5:
			child.pressed = true;

	control_digits_bar.deselect_other_digits(4);
	
	for child in control_digits_bar.get_digit_buttons():
		if child.get_value() == 5:
			assert_false(child.pressed);