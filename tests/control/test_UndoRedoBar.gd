extends "res://addons/gut/test.gd"

var scene: PackedScene = preload("res://scenes/control/UndoRedoBar.tscn");
var bar: UndoRedoBar;

func before_each() -> void:
	bar = scene.instance() as UndoRedoBar;
	add_child(bar);

func after_each() -> void:
	remove_child(bar);

func test_connected_to_child_signals() -> void:
	var buttons: Array = bar.get_buttons();
	for button in buttons:
		assert_true(
			button.is_connected("on_undo_redo_button_click", bar, "_on_undo_redo_button_click"),
			"Connected to child's signal");

func test_get_buttons() -> void:
	var buttons: Array = bar.get_buttons();
	assert_eq(buttons.size(), 2);
	assert_is(buttons[0], UndoRedoButton);