extends "res://addons/gut/test.gd"

var scene: PackedScene = preload("res://scenes/control/ToolModeButton.tscn");
var button: ToolModeButton;

func before_each() -> void:
	button = scene.instance() as ToolModeButton;

func test_emit_touch_signal() -> void:
	watch_signals(button);
	button.emit_touch_signal();
	assert_signal_emitted(button, "on_toolmode_button_click");