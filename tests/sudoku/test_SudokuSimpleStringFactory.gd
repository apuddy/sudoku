extends "res://addons/gut/test.gd"

var factory: SudokuSimpleStringFactory;

func before_each() -> void:
	factory = SudokuSimpleStringFactory.new();

func test_empty_grid_string_to_array() -> void:
	var game: String = ".................................................................................";
	
	var array: Array = factory.parse_simple_string_to_array(game);

	assert_eq(array[50][0], range(1, 10));
	assert_eq(array[40][1], "?");

func test_real_solved_string_to_array() -> void:
	var game: String = "678943512315628497924157683186374259253819764497265831562731948841592376739486125";
	
	var array: Array = factory.parse_simple_string_to_array(game);

	assert_eq(array[27][0], [1]);
	assert_eq(array[27][1], "!");
	assert_eq(array[36][0], [2]);
	assert_eq(array[36][1], "!");

func test_real_puzzle_string_to_array() -> void:
	var game: String = "8......5......69711..752........5.12.4.6.1.8.97.2........324..77245......9......5";
	
	var array: Array = factory.parse_simple_string_to_array(game);

	assert_eq(array[18][0], [1]);
	assert_eq(array[18][1], "!");
	assert_eq(array[27][0], range(1, 10));
	assert_eq(array[27][1], "?");

func test_empty_grid_string_to_grid() -> void:
	var game: String = ".................................................................................";
	
	var grid: SudokuGrid = factory.create_from_string(game);

	assert_eq(Cell.get_candidates(grid.cells[50]), range(1, 10));
	assert_false(Cell.is_filled(grid.cells[50]));

func test_real_solved_string_to_grid() -> void:
	var game: String = "678943512315628497924157683186374259253819764497265831562731948841592376739486125";
	
	var grid: SudokuGrid = factory.create_from_string(game);

	assert_eq(Cell.get_value(grid.cells[27]), 1);
	assert_true(Cell.is_given(grid.cells[27]));
	assert_eq(Cell.get_value(grid.cells[36]), 2);
	assert_true(Cell.is_given(grid.cells[36]));

func test_real_puzzle_string_to_grid() -> void:
	var game: String = "8......5......69711..752........5.12.4.6.1.8.97.2........324..77245......9......5";
	
	var grid: SudokuGrid = factory.create_from_string(game);

	assert_eq(Cell.get_value(grid.cells[18]), 1);
	assert_true(Cell.is_given(grid.cells[18]));
	assert_eq(Cell.get_candidates(grid.cells[27]), [3, 6]);
	assert_false(Cell.is_given(grid.cells[27]));
