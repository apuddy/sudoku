extends "res://addons/gut/test.gd"

var history: StateHistory;

func before_each() -> void:
	history = StateHistory.new();

func test_get_current_state_on_empty_history() -> void:
	assert_null(history.get_current_state());

func test_get_current_state_on_single_history() -> void:
	var state = SudokuGrid.new();
	history.add_new_state(state);
	assert_eq(history.get_current_state(), state);

func test_get_current_state_on_double_history() -> void:
	var stateA = SudokuGrid.new();
	var stateB = SudokuGrid.new();
	history.add_new_state(stateA);
	history.add_new_state(stateB);
	assert_ne(history.get_current_state(), stateA);
	assert_eq(history.get_current_state(), stateB);

func test_can_move_to_previous_state_on_empty_history() -> void:
	assert_false(history.can_move_to_previous_state());

func test_can_move_to_previous_state_on_single_history() -> void:
	history.add_new_state(SudokuGrid.new());
	assert_false(history.can_move_to_previous_state());

func test_can_move_to_previous_state_on_double_history() -> void:
	history.add_new_state(SudokuGrid.new());
	history.add_new_state(SudokuGrid.new());
	assert_true(history.can_move_to_previous_state());

func test_move_to_previous_state_on_empty_history() -> void:
	assert_null(history.move_to_previous_state());

func test_move_to_previous_state_on_single_history() -> void:
	var state = SudokuGrid.new();
	history.add_new_state(state);
	assert_null(history.move_to_previous_state());

func test_move_to_previous_state_on_double_history() -> void:
	var stateA = SudokuGrid.new();
	var stateB = SudokuGrid.new();
	history.add_new_state(stateA);
	history.add_new_state(stateB);
	assert_eq(history.move_to_previous_state(), stateA);

func test_move_to_previous_state_twice_on_double_history() -> void:
	var stateA = SudokuGrid.new();
	var stateB = SudokuGrid.new();
	history.add_new_state(stateA);
	history.add_new_state(stateB);
	assert_eq(history.move_to_previous_state(), stateA);
	assert_null(history.move_to_previous_state());

func test_can_move_to_next_state_on_empty_history() -> void:
	assert_false(history.can_move_to_next_state());

func test_can_move_to_next_state_on_single_history() -> void:
	history.add_new_state(SudokuGrid.new());
	assert_false(history.can_move_to_next_state());

func test_can_move_to_next_state_on_double_history() -> void:
	history.add_new_state(SudokuGrid.new());
	history.add_new_state(SudokuGrid.new());
	history.move_to_previous_state();
	assert_true(history.can_move_to_next_state());

func test_move_to_next_state_on_empty_history() -> void:
	assert_null(history.move_to_next_state());

func test_move_to_next_state_on_single_history() -> void:
	var state = SudokuGrid.new();
	history.add_new_state(state);
	assert_null(history.move_to_next_state());

func test_move_to_next_state_on_double_history() -> void:
	var stateA = SudokuGrid.new();
	var stateB = SudokuGrid.new();
	history.add_new_state(stateA);
	history.add_new_state(stateB);
	history.move_to_previous_state();
	assert_eq(history.move_to_next_state(), stateB);

func test_move_to_previous_state_thrice_on_triple_history() -> void:
	var stateA = SudokuGrid.new();
	var stateB = SudokuGrid.new();
	var stateC = SudokuGrid.new();
	history.add_new_state(stateA);
	history.add_new_state(stateB);
	history.add_new_state(stateC);
	history.move_to_previous_state();
	history.move_to_previous_state();
	assert_eq(history.move_to_next_state(), stateB);
	assert_eq(history.move_to_next_state(), stateC);
	assert_null(history.move_to_next_state());

func test_add_new_state_after_move_backwards() -> void:
	var stateA = SudokuGrid.new();
	var stateB = SudokuGrid.new();
	history.add_new_state(stateA);
	history.add_new_state(stateB);
	history.move_to_previous_state();

	var stateC = SudokuGrid.new();
	history.add_new_state(stateC);

	assert_eq(history.get_current_state(), stateC);