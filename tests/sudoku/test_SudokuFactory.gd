extends "res://addons/gut/test.gd"

var factory: SudokuFactory;

func before_each() -> void:
	factory = SudokuFactory.new();

func test_array_to_grid_empty_grid() -> void:
	var array: Array = [];
	for i in range(81):
		array.push_back([range(1, 10), "?"]);
	
	var grid: SudokuGrid = factory.array_to_grid(array);

	assert_eq(Cell.candidate_count(grid.cells[0]), 9);
	assert_false(Cell.is_filled(grid.cells[0]));

func test_array_to_grid_empty_grid_one_given() -> void:
	var array: Array = [];
	array.push_back([[1], "!"]);
	for i in range(80):
		array.push_back([range(1, 10), "?"]);
	
	var grid: SudokuGrid = factory.array_to_grid(array);

	assert_eq(Cell.get_value(grid.cells[0]), 1);
	assert_true(Cell.is_given(grid.cells[0]));
	assert_false(Cell.is_set(grid.cells[0]));
	assert_eq(Cell.candidate_count(grid.cells[1]), 8);
	assert_false(Cell.is_filled(grid.cells[1]));

func test_array_to_grid_empty_grid_one_set() -> void:
	var array: Array = [];
	array.push_back([[1], "+"]);
	for i in range(80):
		array.push_back([range(1, 10), "?"]);
	
	var grid: SudokuGrid = factory.array_to_grid(array);

	assert_eq(Cell.get_value(grid.cells[0]), 1);
	assert_true(Cell.is_set(grid.cells[0]));
	assert_false(Cell.is_given(grid.cells[0]));
	assert_eq(Cell.candidate_count(grid.cells[1]), 8);
	assert_false(Cell.is_filled(grid.cells[1]));