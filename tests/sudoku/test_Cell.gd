extends "res://addons/gut/test.gd"

var cell: int;

func before_each() -> void:
	cell = Cell.new_cell();

func test_new_cell_is_unfilled() -> void:
	assert_false(Cell.is_filled(cell));

func test_new_cell_has_all_candidates() -> void:
	for candidate in range(1, 10):
		assert_true(Cell.has_candidate(cell, candidate));

func test_new_cell_has_9_candidates() -> void:
	assert_eq(Cell.candidate_count(cell), 9);

func test_remove_candidate() -> void:
	for candidate in range(1, 10):
		cell = Cell.remove_candidate(cell, candidate);
		assert_false(Cell.has_candidate(cell, candidate));

func test_all_candidates_mask() -> void:
	var mask: int = 0;
	for candidate in range(1, 10):
		mask |= Cell.CANDIDATE_MASKS[candidate];
	assert_eq(Cell.ALL_CANDIDATE_MASK, mask)

func test_set_candidates_to_empty() -> void:
	cell = Cell.set_candidates(cell, []);
	assert_eq(Cell.get_candidates(cell).size(), 0);
	assert_eq(Cell.candidate_count(cell), 0)

func test_set_candidates() -> void:
	cell = Cell.set_candidates(cell, [1, 4, 6]);
	assert_eq(Cell.get_candidates(cell).size(), 3);
	assert_eq(Cell.candidate_count(cell), 3)
	assert_true(Cell.has_candidate(cell, 1));
	assert_true(Cell.has_candidate(cell, 4));
	assert_true(Cell.has_candidate(cell, 6));
	assert_false(Cell.has_candidate(cell, 2));

func test_reset_candidates() -> void:
	cell = Cell.set_candidates(cell, []);
	cell = Cell.reset_candidates(cell);
	assert_eq(Cell.candidate_count(cell), 9);

func test_set_value() -> void:
	var value: int = 5;
	cell = Cell.set_value(cell, value);
	assert_eq(Cell.get_value(cell), value);
	assert_true(Cell.is_filled(cell));

func test_unset_value() -> void:
	var value: int = 5;
	cell = Cell.set_value(cell, value);
	cell = Cell.unset_value(cell);
	assert_false(Cell.is_filled(cell));

func test_get_first_candidate() -> void:
	assert_eq(Cell.get_first_candidate(cell), 1);

func test_get_first_candidate_with_less_candidates() -> void:
	cell = Cell.remove_candidate(cell, 1);
	assert_eq(Cell.get_first_candidate(cell), 2);

func test_get_candidates() -> void:
	assert_eq(Cell.get_candidates(cell), [1, 2, 3, 4, 5, 6, 7, 8, 9]);

func test_get_candidates_after_removing_candidate() -> void:
	cell = Cell.remove_candidate(cell, 6);
	assert_eq(Cell.get_candidates(cell), [1, 2, 3, 4, 5, 7, 8, 9]);

func test_given_cell() -> void:
	cell = Cell.set_state(cell, Cell.State.GIVEN);
	assert_true(Cell.is_given(cell));
	assert_false(Cell.is_set(cell));

func test_set_cell() -> void:
	cell = Cell.set_state(cell, Cell.State.SET);
	assert_true(Cell.is_set(cell));
	assert_false(Cell.is_given(cell));

func test_unfilled_cell() -> void:
	cell = Cell.set_state(cell, Cell.State.UNFILLED);
	assert_false(Cell.is_set(cell));
	assert_false(Cell.is_given(cell));