extends "res://addons/gut/test.gd"

var list: SingleList;

func before_each() -> void:
	list = SingleList.new();

func test_empty_list() -> void:
	assert_false(list.has_single());

func test_pop_from_empty_list() -> void:
	assert_eq(list.get_single(), null);

func test_add_single() -> void:
	list.add_single(0, 5);
	assert_eq(list.singles.size(), 1);

func test_pop_single() -> void:
	list.add_single(0, 5);
	assert_eq(list.get_single(), [0, 5]);

func test_remove_single_default_value() -> void:
	list.add_single(0, 5);

	list.remove_single(0);
	assert_eq(list.get_single(), null);

func test_remove_single_with_value() -> void:
	list.add_single(0, 5);

	list.remove_single(0, 5);
	assert_eq(list.get_single(), null);

func test_remove_single_with_wrong_value() -> void:
	list.add_single(0, 5);

	list.remove_single(0, 4);
	assert_eq(list.get_single(), [0, 5]);

func test_add_multiple_singles() -> void:
	list.add_single(0, 5);
	list.add_single(0, 6);
	list.add_single(0, 7);

	assert_eq(list.singles.size(), 1);

func test_clone() -> void:
	list.add_single(0, 5);
	list.add_single(1, 6);
	list.add_single(2, 7);

	var clone: SingleList = list.clone();

	assert_eq(list.singles, clone.singles);