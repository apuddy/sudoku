extends "res://addons/gut/test.gd"

var factory: SudokuFactory;
var grid: SudokuGrid;

func before_each() -> void:
	grid = SudokuGrid.new();

func before_all() -> void:
	factory = SudokuSimpleStringFactory.new();

func test_create_cells() -> void:
	var cells: Array = grid.create_cells();

	assert_eq(cells.size(), 81);
	assert_eq([1, 2, 3, 4, 5, 6, 7, 8, 9], Cell.get_candidates(cells[0]));

func test_create_houses() -> void:
	var houses: Array = grid.create_houses();

	assert_eq(houses.size(), 27);
	assert_eq(houses[0], [0, 1, 2, 3, 4, 5, 6, 7, 8]);
	assert_eq(houses[9], [0, 9, 18, 27, 36, 45, 54, 63, 72]);
	assert_eq(houses[18], [0, 1, 2, 9, 10, 11, 18, 19, 20]);

func test_create_houses_per_cell() -> void:
	var houses: Array = grid.create_houses_per_cell();

	assert_eq(houses.size(), 81);
	assert_eq(houses[0].size(), 3);
	assert_eq(houses[0], [0, 9, 18]);
	assert_eq(houses[1], [0, 10, 18]);
	assert_eq(houses[80], [8, 17, 26]);

func test_create_neighbours() -> void:
	var neighbours: Array = grid.create_neighbours();

	assert_eq(neighbours.size(), 81);
	assert_eq(neighbours[0].size(), 20);
	assert_eq(neighbours[50].size(), 20);

	assert_eq(neighbours[0].sort(), [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 18, 19, 20, 18, 27, 36, 45, 54, 63, 72].sort());

func test_create_house_candidates() -> void:
	var house_candidates: Array = grid.create_house_candidates();

	assert_eq(house_candidates.size(), 27);
	assert_eq(house_candidates[0].size(), 9);
	assert_eq(house_candidates[0], [9, 9, 9, 9, 9, 9, 9, 9, 9]);

func test_create_rows() -> void:
	var rows: Array = grid.create_rows();

	assert_eq(rows.size(), 9);
	assert_eq(rows[0], [0, 1, 2, 3, 4, 5, 6, 7, 8]);
	assert_eq(rows[8], [72, 73, 74, 75, 76, 77, 78, 79, 80]);

func test_create_cols() -> void:
	var cols: Array = grid.create_cols();

	assert_eq(cols.size(), 9);
	assert_eq(cols[0], [0, 9, 18, 27, 36, 45, 54, 63, 72]);
	assert_eq(cols[8], [8, 17, 26, 35, 44, 53, 62, 71, 80]);

func test_create_boxes() -> void:
	var boxes: Array = grid.create_boxes();

	assert_eq(boxes.size(), 9);
	assert_eq(boxes[0], [0, 1, 2, 9, 10, 11, 18, 19, 20]);
	assert_eq(boxes[8], [60, 61, 62, 69, 70, 71, 78, 79, 80]);

func test_solved_count_empty() -> void:
	assert_eq(grid.solved_count(), 0);

func test_set_cell_value() -> void:
	var new_grid: SudokuGrid = grid.set_cell_value(0, 1);

	assert_false(Cell.is_filled(grid.cells[0]));
	assert_true(Cell.is_filled(new_grid.cells[0]));

	assert_eq(Cell.get_value(new_grid.cells[0]), 1);
	assert_false(Cell.has_candidate(new_grid.cells[1], 1));
	assert_false(Cell.has_candidate(new_grid.cells[9], 1));
	assert_false(Cell.has_candidate(new_grid.cells[72], 1));

	assert_eq(new_grid.house_candidates[0][0], 0);
	assert_eq(new_grid.house_candidates[0][1], 8);

func test_set_cell_value_inplace() -> void:
	grid.set_cell_value_inplace(0, 1);

	assert_true(Cell.is_filled(grid.cells[0]));

	assert_eq(Cell.get_value(grid.cells[0]), 1);
	assert_false(Cell.has_candidate(grid.cells[1], 1));
	assert_false(Cell.has_candidate(grid.cells[9], 1));
	assert_false(Cell.has_candidate(grid.cells[72], 1));

	assert_eq(grid.house_candidates[0][0], 0);
	assert_eq(grid.house_candidates[0][1], 8);

func test_set_cell_value_inplace_multiple() -> void:
	grid.set_cell_value_inplace(0, 1);
	grid.set_cell_value_inplace(20, 2);

	assert_false(Cell.has_candidate(grid.cells[9], 1));
	assert_false(Cell.has_candidate(grid.cells[9], 2));

	assert_eq(grid.house_candidates[0][0], 0);
	assert_eq(grid.house_candidates[0][1], 6);
	assert_eq(grid.house_candidates[0][2], 8);

func test_unset_cell_value_inplace_multiple() -> void:
	grid.set_cell_value_inplace(0, 1);
	grid.unset_cell_value_inplace(0);

	assert_false(Cell.is_filled(grid.cells[0]));

	assert_true(Cell.has_candidate(grid.cells[1], 1));
	assert_true(Cell.has_candidate(grid.cells[9], 1));
	assert_true(Cell.has_candidate(grid.cells[72], 1));

	assert_eq(grid.house_candidates[0][0], 9);
	assert_eq(grid.house_candidates[0][1], 9);

func test_remove_candidate_inplace() -> void:
	grid.remove_candidate_inplace(0, 1);

	assert_false(Cell.has_candidate(grid.cells[0], 1));
	assert_true(Cell.has_candidate(grid.cells[1], 1));

	assert_eq(grid.house_candidates[0][0], 8);

func test_add_candidate_inplace() -> void:
	grid.remove_candidate_inplace(0, 1);
	grid.add_candidate_inplace(0, 1);

	assert_true(Cell.has_candidate(grid.cells[0], 1));

	assert_eq(grid.house_candidates[0][0], 9);

func test_unset_cell_value_inplace_bug() -> void:
	grid = factory.create_from_string("...........93678157561489321956742836735821498249315764678953219827136...........");

	assert_true(grid.hidden_single_list.singles.has([5, 9]), "Hidden single should exist");

	grid.unset_cell_value_inplace(11);
	assert_true(grid.hidden_single_list.singles.has([5, 9]), "Hidden single should still exist but does not");

func test_add_candidate_inplace_bug() -> void:
	var factory2: SudokuFactory = SudokuStringFactory.new();
	grid = factory2.create_from_string("23?134?18?2?25?9?47?6?47?2?4?9!3!6!7!8!1!5!7!5!6!1!4!8!9!3!2!1!9!5!6!7!4!2!8!3!6!7!3!5!8!2!1!4!9!8!2!4!9!3!1!5!7!6!4!6!7!8!9!5!3!2!1!9!8!2!7!1!3!6!5?4?35?13?1?24?2?6?47?59?478?");

	assert_true(grid.hidden_single_list.singles.has([5, 9]), "Hidden single should exist");

	grid.add_candidate_inplace(2, 9);
	assert_true(grid.hidden_single_list.singles.has([5, 9]), "Hidden single should still exist but does not");

func test_recalculate_house_candidates() -> void:
	grid = factory.create_from_string("8......5......69711..752........5.12.4.6.1.8.97.2........324..77245......9......5");
	var original_house_candidates: Array = grid.house_candidates.duplicate(true);

	grid.recalculate_house_candidates();

	assert_eq(grid.house_candidates, original_house_candidates);

func test_recalculate_hidden_singles() -> void:
	grid = factory.create_from_string("8......5......69711..752........5.12.4.6.1.8.97.2........324..77245......9......5");
	var original_singles: Array = grid.hidden_single_list.singles.duplicate(true);

	grid.recalculate_hidden_singles();

	var new_singles: Array = grid.hidden_single_list.singles;

	var matches: bool = true;
	for single in original_singles:
		if !new_singles.has(single) and !Cell.is_filled(grid.cells[single[0]]):
			matches = false;
	for single in new_singles:
		if !original_singles.has(single) and !Cell.is_filled(grid.cells[single[0]]):
			matches = false;

	assert_true(matches);

func test_recalculate_naked_singles() -> void:
	grid = factory.create_from_string("8......5......69711..752........5.12.4.6.1.8.97.2........324..77245......9......5");
	var original_singles: Array = grid.naked_single_list.singles.duplicate(true);

	grid.recalculate_naked_singles();

	var new_singles: Array = grid.naked_single_list.singles;

	var matches: bool = true;
	for single in original_singles:
		if !new_singles.has(single) and !Cell.is_filled(grid.cells[single[0]]):
			matches = false;
	for single in new_singles:
		if !original_singles.has(single) and !Cell.is_filled(grid.cells[single[0]]):
			matches = false;

	assert_true(matches);

func test_houses_containing_cells() -> void:
	var subsets: Dictionary = {
		[0]: [0, 9, 18],
		[0, 1, 2]: [0, 18],
		[0, 9, 10]: [18],
		[0, 2, 8]: [0],
		[0, 9, 72]: [9]
	};

	for subset in subsets:
		var houses = grid.houses_containing_cells(subset);
		assert_eq(houses, subsets[subset]);