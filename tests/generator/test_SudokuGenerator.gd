extends "res://addons/gut/test.gd"

var factory: SudokuFactory;
var grid: SudokuGrid;
var solver: BacktrackSolver;
var generator: SudokuGenerator;

func before_each() -> void:
	grid = SudokuGrid.new();
	solver = BacktrackSolver.new();

func before_all() -> void:
	factory = SudokuSimpleStringFactory.new();
	generator = SudokuGenerator.new();

func test_create_puzzle() -> void:
	var solved_puzzle: String = "318259467249367815756148932195674283673582149824931576467895321982713654531426798";

	var puzzle: SudokuGrid = generator.create_sudoku_iterate(solved_puzzle);

	assert_true(factory.create_from_string(puzzle.as_simple_string()).hidden_single_list.singles.has([5, 9]));
	assert_true(puzzle.hidden_single_list.singles.has([5, 9]), "Hidden single doesn't exist when it should");

func test_generate_bug() -> void:
	grid = factory.create_from_string(".....6.1.5..2......6...945837......6....3....9......854835...2......3..9.2.4.....");
	var solved_singles: SudokuGrid = solver.apply_constraints(grid);

	var index: int = 69;
	var cell: int = solved_singles.cells[index];
	assert_true(Cell.is_filled(cell), "Cell should be filled");
	assert_eq(Cell.get_value(cell), 8, "Cell should have value 8");
