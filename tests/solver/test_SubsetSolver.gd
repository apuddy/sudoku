extends "res://addons/gut/test.gd"

var factory: SudokuFactory;
var grid: SudokuGrid;
var solver: BacktrackSolver;

func before_each() -> void:
	grid = SudokuGrid.new();
	solver = BacktrackSolver.new();

func before_all() -> void:
	factory = SudokuSimpleStringFactory.new();

func test_find_naked_pairs() -> void:
	var games: Dictionary = {
		".8...394...5..461.....9...74..91..8....3....4.5.472.965...3.....312..4...7964..3.": [8, 25],
		"5..893..4.48.7....96.14528...9......7...3...1......5....6927..889..1.72.4..358..6": [12, 14],
		"....2..8.3.2..89.6548369217..1687...8..9........1428..7965...2818529.7.....87....": [40, 41],
		"872134569153968427946725831..5.461.8..1.9.64.6.4.1.2..217459386569.8.714438671952": [30, 66],
		"...974.589..538..6.8.612..92...458......87..28.4269..77..851.2.3..426..5.2.793...": [30, 35]
	};

	for game in games:
		var grid: SudokuGrid = factory.create_from_string(game);
		grid.recalculate();
		var tuple: NakedSubset = SubsetSolver.find_naked_subset(grid, 2);

		assert_eq(tuple.indices, games[game]);

func test_find_naked_pairs_none_exist() -> void:
	var games: Array = [
		"295.8...36...15.............2.6.4.5...6.5.2...5.1.9.6.............57...93...9.845",
		".3.95....5...7..8..82...1.......4...8..5.1..2...7.......6...75..9..6...3....45.1."
	];

	for game in games:
		var grid: SudokuGrid = factory.create_from_string(game);
		var tuple: NakedSubset = SubsetSolver.find_naked_subset(grid, 2);

		assert_null(tuple);

func test_find_naked_pairs_no_eliminations() -> void:
	var games: Array = [
		"5218936743486721599..14528321956483778523946163478159215.9273488934167254.2358916"
	];

	for game in games:
		var grid: SudokuGrid = factory.create_from_string(game);
		var tuple: NakedSubset = SubsetSolver.find_naked_subset(grid, 2);

		assert_null(tuple);

func test_find_naked_triples() -> void:
	var games: Dictionary = {
		".743156..1..497..39358264174..572...3276819456519437287...3....5..768..4..3.5..7.": [72, 75, 78],
		"75...42.8.468...5.8.2...6472.47.68...8..4..6.....894.25....8....2...398.4.81...26": [9, 15, 17],
		"3..87..4.4.713......8694...543219786786543912921768.5....956...6..3812..83.427..1": [2, 5, 8],
		"4.1......3.87.2164.62..15....4..8...217...836...1..4....93...7.1.58.7.49........8": [39, 40, 41],
		"3....4869..46...3..6...347.459167328617832954238459716..67...43...3.869.1.3..6.87": [14, 15, 17]
	};

	for game in games:
		var grid: SudokuGrid = factory.create_from_string(game);
		grid.recalculate();
		var tuple: NakedSubset = SubsetSolver.find_naked_subset(grid, 3);

		assert_eq(tuple.indices, games[game]);

func test_naked_subset_cannot_eliminate() -> void:
	var games: Dictionary = {
		"5218936743486721599..14528321956483778523946163478159215.9273488934167254.2358916": [19, 20],
		"943..7.651563...47728...19353147.689467983512289651374372169458695.34721814725936": [3, 14],
		"185.6.....7.485169.6.2.37..89.....1...61985...5.....82..86...7.5....9.4.....7.8.5": [9, 11]
	};

	for game in games:
		var grid: SudokuGrid = factory.create_from_string(game);
		var union: int = grid.candidates_in_cell_union(games[game]);
		assert_false(SubsetSolver.naked_subset_can_eliminate(grid, games[game], union));

func test_naked_subset_can_eliminate() -> void:
	var games: Dictionary = {
		".8...394...5..461.....9...74..91..8....3....4.5.472.965...3.....312..4...7964..3.": [8, 25],
		"5..893..4.48.7....96.14528...9......7...3...1......5....6927..889..1.72.4..358..6": [12, 14],
		"....2..8.3.2..89.6548369217..1687...8..9........1428..7965...2818529.7.....87....": [40, 41],
		"872134569153968427946725831..5.461.8..1.9.64.6.4.1.2..217459386569.8.714438671952": [30, 66],
		"...974.589..538..6.8.612..92...458......87..28.4269..77..851.2.3..426..5.2.793...": [30, 35]
	};

	for game in games:
		var grid: SudokuGrid = factory.create_from_string(game);
		var union: int = grid.candidates_in_cell_union(games[game]);
		assert_true(SubsetSolver.naked_subset_can_eliminate(grid, games[game], union));

func test_find_hidden_subsets() -> void:
	var games: Dictionary = {
		"97..13....2.67..9.4.65923..791.56.3..4273195....2.97....91274.32349.5.....73..529": [6, 7, 8, 17],
		"281459376359716284647238159...3276.....864....6.591...93.182.67.286739..176945832": [38, 47],
		"6...2.1.38..3.72463....1.5973...25..4..5.6...9.51...2414.2.....5..7194.22.7.8..15": [1, 10, 19, 37],
		"375192846619..8572824.7.3919.2....5876185.92.5.8....6.4.7...6151.67..2392.3.61487": [48, 49, 50, 51],
		"974..13822613845975..7296146..9....884.2.5.691.9.....539647285171..962434.21..976": [31, 33],
		"974..13822613845975..7296146..9....884.2.5.691.9.....539647285171..962434.21..976*": [48, 49, 50, 51] # added a * to prevent duplicate key
	};

	for game in games:
		var grid: SudokuGrid = factory.create_from_string(game);
		grid.recalculate();
		var tuple: HiddenSubset = SubsetSolver.find_hidden_subset(grid, games[game].size());

		assert_eq(tuple.indices, games[game]);
