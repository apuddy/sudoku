extends VBoxContainer

var grids: int = 0;

var factory: SudokuFactory;
var game: String;
var solver: BacktrackSolver;
var start: int;
var last: int;
var generator: SudokuGenerator;

func _init() -> void:
	factory = SudokuSimpleStringFactory.new();
	game = ".3..21..6.....438..7.......8..9...3.31.....54.5...8..1.......6..867.....7..34..2.";
	solver = BacktrackSolver.new();

func _ready() -> void:
	start = OS.get_ticks_msec();
	last = OS.get_ticks_msec();
	generator = SudokuGenerator.new();

func _process(delta: float) -> void:
	var new_grid: SudokuGrid = factory.create_from_string(game);
	($Unsolved/GridView as GridView).grid = new_grid;
	($Unsolved/GridView as GridView).redraw();
	($Solved/GridView as GridView).grid = solver.solve(new_grid);
	($Solved/GridView as GridView).redraw();

	print(new_grid.as_simple_string());

	grids += 1;

	var time: int = OS.get_ticks_msec() - start;
	var this_time: int = OS.get_ticks_msec() - last;
	last = OS.get_ticks_msec();
	print(str(this_time), " ms");
	($Label as Label).text = String(grids) + " in " + String(time/1000.0) + " secs (" + String(time / grids) + "ms/grid)";