extends SolveStep

class_name HiddenSubset

var candidates: Array = [];
var indices: Array = [];

func _init(indices: Array, candidates: Array) -> void:
	self.indices = indices;
	self.candidates = candidates;

func get_removals(grid: SudokuGrid) -> Array:
	var removals: Array = [];
	var removal_candidates: Array = Cell.get_all_candidates();
	
	for candidate in candidates:
		removal_candidates.erase(candidate);

	for index in indices:
		removals.push_back(Removal.new(index, removal_candidates));

	return removals;