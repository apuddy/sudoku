extends Reference

class_name BacktrackSolver

func generate_solved(prototype: SudokuGrid) -> SudokuGrid:
	return solve(prototype, true);

func solve(grid: SudokuGrid, random: bool = false, uniqueness: bool = false) -> SudokuGrid:
	var new_grid: SudokuGrid = grid.clone();
	
	if !new_grid.is_valid():
		return null;

	var stack: Array = [apply_constraints(new_grid)];
	var solution_found: SudokuGrid;
	
	while (!stack.empty()):
		var state: SudokuGrid = stack.pop_back();
		
		var cell_index: int = find_unsolved_cell(state);
		if cell_index == -1 and state.is_valid():
			if uniqueness:
				if solution_found:
					return null;
				else:
					solution_found = state;
			else:
				return state;
		
		var candidates: Array = Cell.get_all_candidates();
		if random:
			candidates.shuffle();
		
		for candidate in candidates:
			if Cell.has_candidate(state.cells[cell_index], candidate):
				var next_state: SudokuGrid = state.set_cell_value(cell_index, candidate);
				apply_constraints(next_state);

				if next_state.is_valid():
					stack.push_back(next_state);
	
	if uniqueness:
		return solution_found;
	else:
		return null;

func is_solvable(grid: SudokuGrid) -> bool:
	return solve(grid) != null;

func is_uniquely_solvable(grid: SudokuGrid) -> bool:
	return solve(grid, false, true) != null;

func find_unsolved_cell(grid: SudokuGrid) -> int:
	for i in range(grid.cells.size()):
		var cell: int = grid.cells[i];
		if !Cell.is_filled(cell):
			return i;
	return -1;

func apply_constraints(grid: SudokuGrid) -> SudokuGrid:
	var single: Array = grid.get_single();
	while single != []:
		set_single(grid, single);
		single = grid.get_single();
	return grid;

func set_single(grid: SudokuGrid, single: Array) -> SudokuGrid:
	var index: int = single[0];
	var value: int = single[1];
	
	grid.set_cell_value_inplace(index, value);
	
	return grid;
