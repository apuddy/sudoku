extends Reference

class_name SubsetSolver

static func find_naked_subset(grid: SudokuGrid, size: int) -> NakedSubset:
	for house_index in range(grid.houses.size()):
		var house = grid.houses[house_index];

		var indices: Array = [];
		for cell_index in house:
			var cell: int = grid.cells[cell_index];
			if !Cell.is_filled(cell) and Cell.candidate_count(cell) <= size and Cell.candidate_count(cell) > 1:
				indices.push_back(cell_index);
		
		var permutations = Permutations.permutations_of_length_k(indices, size);

		for permutation in permutations:
			var union: int = grid.candidates_in_cell_union(permutation);
			
			if Cell.candidate_count(union) == size and naked_subset_can_eliminate(grid, permutation, union):
				return NakedSubset.new(permutation, Cell.get_candidates(union));
	
	return null;

static func find_hidden_subset(grid: SudokuGrid, size: int) -> HiddenSubset:
	for house_index in range(grid.houses.size()):
		var house: Array = grid.houses[house_index];

		# Check that number of unset cells is not greater than subset size
		var unset_cells_in_house: int = 0;
		for cell_index in house:
			if !Cell.is_filled(grid.cells[cell_index]):
				unset_cells_in_house += 1;
		
		if unset_cells_in_house <= size:
			continue;

		var set: Array = [];
		var index_set: Array = []; # nth element is a bitmask that indicates what indices the nth candidate can be placed in
		for candidate in Cell.get_all_candidates():
			index_set.push_back(0);

			var count: int = grid.house_candidates[house_index][candidate - 1];
			if count >= 1 and count <= size:
				set.push_back(candidate);

				for cell_in_house_index in range(house.size()):
					var cell_index: int = house[cell_in_house_index];
					var cell = grid.cells[cell_index];
					if Cell.has_candidate(cell, candidate):
						index_set[candidate - 1] |= Cell.CANDIDATE_MASKS[cell_in_house_index + 1];

		
		var permutations = Permutations.permutations_of_length_k(set, size);

		for permutation in permutations:
			var index_union: int = 0;
			var candidate_union: int = 0;
			for candidate in permutation:
				index_union |= index_set[candidate - 1];
				candidate_union |= Cell.CANDIDATE_MASKS[candidate];
			
			if Cell.candidate_count(index_union) == size and hidden_subset_can_eliminate(grid, house_index, candidate_union, index_union):
				# Convert the union of in-house indices to grid indices
				var cell_in_house_indices: Array = Cell.get_candidates(index_union);
				var indices: Array = [];
				for index in cell_in_house_indices:
					indices.push_back(house[index - 1]);

				return HiddenSubset.new(indices, Cell.get_candidates(candidate_union));

	return null;

static func naked_subset_can_eliminate(grid: SudokuGrid, subset: Array, union: int) -> bool:
	var houses = grid.houses_containing_cells(subset);

	for candidate in Cell.get_candidates(union):
		var candidate_count = 0;
		for index in subset:
			if Cell.has_candidate(grid.cells[index], candidate):
				candidate_count += 1;
	
		for house in houses:
			if grid.house_candidates[house][candidate - 1] > candidate_count:
				return true;
	
	return false;

static func hidden_subset_can_eliminate(grid: SudokuGrid, house: int, candidate_union: int, cell_union: int) -> bool:
	for cell_in_house_index in Cell.get_candidates(cell_union):
		var cell_index = grid.houses[house][cell_in_house_index];
		var cell = grid.cells[cell_index];
		
		if !Cell.has_only_candidates_mask(cell, candidate_union):
			return true;

	return false;