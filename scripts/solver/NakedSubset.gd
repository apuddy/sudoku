extends SolveStep

class_name NakedSubset

var candidates: Array = [];
var indices: Array = [];

func _init(indices: Array, candidates: Array) -> void:
	self.indices = indices;
	self.candidates = candidates;

func get_removals(grid: SudokuGrid) -> Array:
	var houses: Array = grid.houses_containing_cells(indices);

	var removals: Array = [];

	for index in houses:
		if indices.has(index):
			removals.push_back(Removal.new(index, Cell.get_all_candidates()));
		else:
			removals.push_back(Removal.new(index, candidates));

	return removals;