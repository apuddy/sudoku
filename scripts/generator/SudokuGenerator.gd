extends Reference

class_name SudokuGenerator

var factory: SudokuFactory;
var solver: BacktrackSolver;
var branching_factor: int = 5;

func _init() -> void:
	factory = SudokuSimpleStringFactory.new();
	solver = BacktrackSolver.new();

func create_sudoku_iterate(game: String = "") -> SudokuGrid:
	var indices: Array = range(0, 41);

	var solved_grid: SudokuGrid;
	if game != "":
		solved_grid = factory.create_from_string(game);
	else:
		solved_grid = solver.generate_solved(factory.prototype);
		indices.shuffle();

	var state: SudokuGrid = solved_grid.clone();

	for index in indices:
		var next_state: SudokuGrid = state.clone();

		next_state.unset_cell_value_inplace(index);
		next_state.unset_cell_value_inplace(80 - index);

		if solver.is_uniquely_solvable(next_state):
			state = next_state;
	
	return state;

func create_non_trivial_iterate() -> SudokuGrid:
	var grid: SudokuGrid = create_sudoku_iterate();
	var progressed: SudokuGrid = solver.apply_constraints(grid.clone());

	while progressed.solved_cell_count == 81:
		grid = create_sudoku_iterate();
		progressed = solver.apply_constraints(grid.clone());

	return grid;
