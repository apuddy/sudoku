extends GridContainer

class_name GridView

signal cell_pressed(index);

var grid: SudokuGrid setget set_grid, get_grid;
var highlight_digit: int = -1;

func _ready() -> void:
	grid = SudokuGrid.new();

	for box in range(0, 9):
		var box_view: Container = find_node("Box" + String(box)) as Container;
		for i in range(0, 9):
			var cell_view: CellView = box_view.find_node("Cell" + String(i)) as CellView;
			cell_view.index = grid.get_box_indices(box)[i];
			cell_view.connect("cell_pressed", self, "_cell_pressed");

func redraw() -> void:
	for box in range(0, 9):
		var box_view: Container = find_node("Box" + String(box)) as Container;
		for i in range(0, 9):
			var cell_view: CellView = box_view.find_node("Cell" + String(i));
			var cell_index: int = grid.get_box_indices(box)[i];
			var cell: int = grid.cells[cell_index];
			cell_view.redraw(cell, highlight_digit);

func set_grid(value: SudokuGrid) -> void:
	grid = value;
	redraw();

func get_grid() -> SudokuGrid:
	return grid;

func _cell_pressed(index: int) -> void:
	emit_signal("cell_pressed", index);
