extends TouchButton

class_name CellView

signal cell_pressed(index);

var index: int;
var cell: int;
var highlight: int;

func redraw(cell: int, highlight_digit: int = -1) -> void:
	self.cell = cell;
	self.highlight = highlight_digit
	hide_value();
	hide_candidates();
	
	if Cell.is_filled(cell):
		show_value(Cell.get_value(cell));
	else:
		for candidate in Cell.get_candidates(cell):
			show_candidate(candidate);
	
	set_highlight(highlight_digit);

func show_value(value: int) -> void:
	($Label as Label).modulate = Color(1, 1, 1, 1);
	($Label as Label).text = String(value);

func show_candidate(candidate: int) -> void:
	var candidate_label: Label = $Candidates.find_node("Candidate" + String(candidate)) as Label;
	candidate_label.modulate = Color(1, 1, 1, 1);
	
func hide_candidates() -> void:
	for candidate_label in $Candidates.get_children():
		(candidate_label as Label).modulate = Color(1, 1, 1, 0);
	
func hide_value() -> void:
	($Label as Label).modulate = Color(1, 1, 1, 0);

func set_highlight(highlight_digit: int) -> void:
	if Cell.is_filled(cell):
		pressed = false;
	else:
		pressed = Cell.has_candidate(cell, highlight_digit);

func emit_touch_signal() -> void:
	emit_signal("cell_pressed", index);

func _on_CellView_pressed() -> void:
	set_highlight(highlight);
