extends Reference

class_name Permutations

static func enumerate_n_choose_k(n: int, k: int) -> Array:
	return permutations_of_length_k(range(0, n), k);

static func permutations_of_length_k(digits: Array, k: int) -> Array:
	var permutations: Array = [];
	var tuple: Array = [];
	tuple.resize(k);
	var stack: Array = [0];

	while stack.size() > 0:
		var tuple_index: int = stack.size() - 1;

		for digit_index in range(stack.pop_back(), digits.size()):
			tuple[tuple_index] = digits[digit_index];
			tuple_index += 1;
			stack.push_back(digit_index + 1);

			if tuple_index == k:
				permutations.push_back(tuple.duplicate());
				break;
	
	return permutations;