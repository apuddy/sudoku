extends TouchButton

class_name ToolModeButton

enum ToolMode {
	VALUE_ADD, VALUE_REMOVE,
	CANDIDATE_ADD, CANDIDATE_REMOVE
};

export(ToolMode) var tool_mode;
signal on_toolmode_button_click(node, mode);

func emit_touch_signal() -> void:
	emit_signal("on_toolmode_button_click", self, tool_mode);