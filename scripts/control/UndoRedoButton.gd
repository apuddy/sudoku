extends TouchButton

class_name UndoRedoButton

signal on_undo_redo_button_click(change)

export var change: int = 0;

func emit_touch_signal() -> void:
	emit_signal("on_undo_redo_button_click", change);
