extends TouchButton

class_name ControlDigit

export var _value: int setget set_value, get_value;
signal selected_control_digit(value);

func set_value(value: int) -> void:
	text = String(value);
	_value = value;

func get_value() -> int:
	return _value;

func emit_touch_signal() -> void:
	emit_signal("selected_control_digit", get_value());
