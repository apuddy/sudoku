extends CenterContainer

class_name ControlDigitsBar
signal selected_digit(value);

var current_digit: int = -1;

func _ready() -> void:
	for child in get_digit_buttons():
		if child is ControlDigit:
			child.connect("selected_control_digit", self, "_selected_control_digit");

func _selected_control_digit(value: int) -> void:
	if value == current_digit:
		value = -1;
	current_digit = value;
	deselect_other_digits(value);
	emit_signal("selected_digit", value);
	
func deselect_other_digits(value: int) -> void:
	for child in get_digit_buttons():
		if child is ControlDigit and child.get_value() != value:
			child.pressed = false;

func get_digit_buttons() -> Array:
	return $HBoxContainer.get_children();

func update_digit_counts(grid: SudokuGrid) -> void:
	for child in get_digit_buttons():
		if child is ControlDigit:
			child.disabled = grid.get_solved_count_per_value(child.get_value()) == grid.GRID_SIZE;