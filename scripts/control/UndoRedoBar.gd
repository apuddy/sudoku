extends CenterContainer

class_name UndoRedoBar

signal on_undo_redo_button_click(change);

func _ready() -> void:
	for button in get_buttons():
		(button as UndoRedoButton).connect("on_undo_redo_button_click", self, "_on_undo_redo_button_click");

func _on_undo_redo_button_click(change: int) -> void:
	emit_signal("on_undo_redo_button_click", change);

func get_buttons() -> Array:
	return $HBoxContainer.get_children();
