extends CenterContainer

class_name ToolBar

signal on_toolmode_button_click(mode);

func _ready() -> void:
	for button in get_tool_buttons():
		button.connect("on_toolmode_button_click", self, "_on_toolmode_button_click");

func _on_toolmode_button_click(node: ToolModeButton, mode) -> void:
	deselect_other_modes(mode);
	emit_signal("on_toolmode_button_click", mode);
	
func deselect_other_modes(mode) -> void:
	for child in get_tool_buttons():
		if child is ToolModeButton and child.tool_mode != mode:
			child.pressed = false;

func get_tool_buttons() -> Array:
	return $ButtonGrid.get_children();