extends CenterContainer

class_name Game

var factory: SudokuSimpleStringFactory;
var solver: BacktrackSolver;
var generator: SudokuGenerator;

var selected_digit: int = -1;
var current_mode = ToolModeButton.ToolMode.VALUE_ADD;

var game_state_history: StateHistory;

var thread: Thread = Thread.new();

func _ready() -> void:
	randomize();
	factory = SudokuSimpleStringFactory.new();
	solver = BacktrackSolver.new();
	generator = SudokuGenerator.new();
	game_state_history = StateHistory.new();
	set_current_grid(generator.create_sudoku_iterate());

	find_node("Puzzle").connect("cell_pressed", self, "_on_press_cell");
	find_node("ControlDigitsBar").connect("selected_digit", self, "_selected_digit");
	find_node("UndoRedoBar").connect("on_undo_redo_button_click", self, "_on_undo_redo_pressed");
	find_node("ToolBar").connect("on_toolmode_button_click", self, "_on_toolmode_changed");

func set_buttons(onoff: bool) -> void:
	(find_node("GenerateButton") as Button).disabled = onoff;
	(find_node("SolveButton") as Button).disabled = onoff;
	(find_node("SinglesButton") as Button).disabled = onoff;

func _selected_digit(value: int) -> void:
	selected_digit = value;
	(find_node("Puzzle") as GridView).highlight_digit = selected_digit;
	(find_node("Puzzle") as GridView).redraw();

func _on_GenerateButton_pressed() -> void:
	if thread.is_active():
		return;

	thread.start(self, "create_new_puzzle");
	set_buttons(true);

func create_new_puzzle(unused) -> void:
	var new_grid: SudokuGrid = generator.create_sudoku_iterate();
	call_deferred("finished_update_puzzle", new_grid);

func finished_update_puzzle(new_grid: SudokuGrid) -> void:
	thread.wait_to_finish();
	if new_grid == null:
		print("unsolvable");
	else:
		set_current_grid(new_grid);
	set_buttons(false);

func _on_SolveButton_pressed() -> void:
	if thread.is_active():
		return;

	thread.start(self, "solve_puzzle", game_state_history.get_current_state().clone());
	set_buttons(true);

func solve_puzzle(grid: SudokuGrid) -> void:
	var new_grid: SudokuGrid = solver.solve(grid);
	call_deferred("finished_update_puzzle", new_grid);

func _on_SinglesButton_pressed() -> void:
	if thread.is_active():
		return;

	if !solver.is_uniquely_solvable(get_current_grid()):
		return;

	thread.start(self, "apply_constraints", game_state_history.get_current_state().clone());
	set_buttons(true);

func apply_constraints(grid: SudokuGrid) -> void:
	var new_grid: SudokuGrid = solver.apply_constraints(grid);
	call_deferred("finished_update_puzzle", new_grid);

func _on_press_cell(index: int) -> void:
	match current_mode:
		ToolModeButton.ToolMode.VALUE_ADD:
			press_cell_value_add(index);
		ToolModeButton.ToolMode.VALUE_REMOVE:
			press_cell_value_remove(index);
		ToolModeButton.ToolMode.CANDIDATE_ADD:
			press_cell_candidate_add(index);
		ToolModeButton.ToolMode.CANDIDATE_REMOVE:
			press_cell_candidate_remove(index);

func press_cell_value_add(index: int) -> void:
	var cell: int = get_current_grid().cells[index];

	if Cell.is_filled(cell):
		return;
	
	if Cell.has_candidate(cell, selected_digit):
		var new_grid: SudokuGrid = get_current_grid().set_cell_value(index, selected_digit);
		set_current_grid(new_grid);

func press_cell_value_remove(index: int) -> void:
	var cell: int = get_current_grid().cells[index];

	if !Cell.is_filled(cell):
		return;
	
	var new_grid: SudokuGrid = get_current_grid().unset_cell_value(index);
	set_current_grid(new_grid);

func press_cell_candidate_add(index: int) -> void:
	var cell: int = get_current_grid().cells[index];
	var new_grid: SudokuGrid;

	if Cell.is_filled(cell):
		return;
	
	if !Cell.has_candidate(cell, selected_digit):
		new_grid = get_current_grid().add_candidate(index, selected_digit);
		set_current_grid(new_grid);

func press_cell_candidate_remove(index: int) -> void:
	var cell: int = get_current_grid().cells[index];
	var new_grid: SudokuGrid;

	if Cell.is_filled(cell):
		return;
	
	if Cell.has_candidate(cell, selected_digit):
		new_grid = get_current_grid().remove_candidate(index, selected_digit);
		set_current_grid(new_grid);

func set_current_grid(new_grid: SudokuGrid) -> void:
	game_state_history.add_new_state(new_grid);
	update_grid_to_current_state();

func undo() -> void:
	game_state_history.move_to_previous_state();
	update_grid_to_current_state();

func redo() -> void:
	game_state_history.move_to_next_state();
	update_grid_to_current_state();

func update_grid_to_current_state() -> void:
	var grid: SudokuGrid = game_state_history.get_current_state();
	(find_node("Puzzle") as GridView).set_grid(grid);
	(find_node("ControlDigitsBar") as ControlDigitsBar).update_digit_counts(get_current_grid());

func get_current_grid() -> SudokuGrid:
	return game_state_history.get_current_state();

func _on_undo_redo_pressed(change: int) -> void:
	if change < 0 and game_state_history.can_move_to_previous_state():
		undo();
	elif change > 0 and game_state_history.can_move_to_next_state():
		redo();

func _on_toolmode_changed(mode) -> void:
	current_mode = mode;