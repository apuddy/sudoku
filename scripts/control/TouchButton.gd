extends Button

class_name TouchButton

func _ready() -> void:
	connect("gui_input", self, "_on_TouchButton_gui_input");

func _on_TouchButton_gui_input(event: InputEventScreenTouch) -> void:
	if not event:
		return;

	var local_rect: Rect2 = get_rect();
	var local_touch_position: Vector2 = event.xformed_by(get_transform()).position;

	if !event.pressed and local_rect.has_point(local_touch_position):
		emit_touch_signal();

# Virtual method
func emit_touch_signal() -> void:
	pass;