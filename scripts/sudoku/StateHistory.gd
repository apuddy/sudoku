extends Reference

class_name StateHistory

var states: Array = [];
var current_state: int = -1;

func get_current_state() -> SudokuGrid:
	if current_state >= 0 and current_state < states.size():
		return states[current_state];
	else:
		return null;

func can_move_to_previous_state() -> bool:
	return current_state > 0;

func can_move_to_next_state() -> bool:
	return current_state < states.size() - 1;

func move_to_previous_state() -> SudokuGrid:
	if can_move_to_previous_state():
		current_state -= 1;
		return states[current_state];
	else:
		return null;

func move_to_next_state() -> SudokuGrid:
	if can_move_to_next_state():
		current_state += 1;
		return states[current_state];
	else:
		return null;

func add_new_state(new_state: SudokuGrid) -> void:
	if can_move_to_next_state():
		delete_next_states();
	
	states.push_back(new_state);
	current_state += 1;
	
func delete_next_states() -> void:
	states.resize(current_state + 1);
