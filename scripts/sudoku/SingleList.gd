extends Reference

class_name SingleList

var singles: Array = [];

func has_single() -> bool:
	return singles.size() > 0;

func get_single() -> Array:
	return singles.pop_front();

func add_single(index: int, value: int) -> void:
	for single in singles:
		if single[0] == index:
			return;
	
	singles.push_back([index, value]);

func remove_single(index: int, value: int = 0) -> void:
	for single in singles:
		if single[0] == index and (value == 0 or single[1] == value):
			singles.erase(single);
			return;

func clone() -> SingleList:
	var copy: SingleList = get_script().new() as SingleList;
	for single in singles:
		copy.singles.push_back([single[0], single[1]]);
	return copy;