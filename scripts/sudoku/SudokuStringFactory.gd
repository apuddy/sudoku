extends SudokuFactory

class_name SudokuStringFactory

func create_from_string(game: String) -> SudokuGrid:
	var arr: Array = parse_string_to_array(game);
	var grid: SudokuGrid = array_to_grid(arr);
	
	return grid;

static func parse_string_to_array(game: String) -> Array:
	var data: Array = [];
	var buffer: Array = [];

	for c in game:
		if c == "?" or c == "!" or c == "+":
			data.push_back([buffer, c]);
			buffer = [];
		else:
			buffer.push_back(c.to_int());
			pass;
	
	return data;
