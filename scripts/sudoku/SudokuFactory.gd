extends Reference

class_name SudokuFactory

var prototype: SudokuGrid;

func _init() -> void:
	prototype = SudokuGrid.new();

func create_from_string(string: String) -> SudokuGrid:
	return null;

func array_to_grid(arr: Array) -> SudokuGrid:
	var grid: SudokuGrid = prototype.clone();
	
	for i in range(SudokuGrid.GRID_SIZE * SudokuGrid.GRID_SIZE):
		var cell: int = grid.cells[i];
		var data: Array = arr[i];

		cell = Cell.set_candidates(cell, data[0]);
		if data[1] == "!":
			cell = Cell.set_candidates(cell, range(1, SudokuGrid.GRID_SIZE + 1));
		elif data[1] == "+":
			cell = Cell.set_candidates(cell, range(1, SudokuGrid.GRID_SIZE + 1));

	for i in range(SudokuGrid.GRID_SIZE * SudokuGrid.GRID_SIZE):
		var cell: int = grid.cells[i];
		var data: Array = arr[i];

		if data[1] == "!":
			grid.set_cell_value_inplace(i, data[0][0], Cell.State.GIVEN, false);
		elif data[1] == "+":
			grid.set_cell_value_inplace(i, data[0][0], Cell.State.SET, false);

	grid.recalculate();

	return grid;
