extends SudokuFactory

class_name SudokuSimpleStringFactory

func create_from_string(game: String) -> SudokuGrid:
	var arr: Array = parse_simple_string_to_array(game);
	var grid: SudokuGrid = array_to_grid(arr);
	return grid;

static func parse_simple_string_to_array(game: String) -> Array:
	var data: Array = [];

	for c in game:
		if c == ".":
			data.push_back([Cell.get_all_candidates(), "?"]);
		else:
			data.push_back([[c.to_int()], "!"]);
	
	return data;
