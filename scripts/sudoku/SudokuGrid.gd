extends Reference

class_name SudokuGrid

const BOX_SIZE = 3;
const GRID_SIZE = 9;
const BOX_CORNERS = [0, 3, 6, 27, 30, 33, 54, 57, 60];
const BOX_OFFSETS = [0, 1, 2, 9, 10, 11, 18, 19, 20];

var houses: Array = [];
var cells: Array = [];
var houses_per_cell: Array = []
var neighbours: Array = [];
var house_candidates: Array = [];

var hidden_single_list: SingleList;
var naked_single_list: SingleList;
var solved_cell_count: int = 0;

var valid: bool = true;

###########
# Construction
###########
func _init(cells: Array = [], houses: Array = [], houses_per_cell: Array = [], neighbours: Array = [], house_candidates: Array = []) -> void:
	self.cells = cells if cells != [] else create_cells();
	self.houses = houses if houses != [] else create_houses();
	self.houses_per_cell = houses_per_cell if houses_per_cell != [] else create_houses_per_cell();
	self.neighbours = neighbours if neighbours != [] else create_neighbours();
	self.house_candidates = house_candidates if house_candidates != [] else create_house_candidates();
	self.hidden_single_list = SingleList.new();
	self.naked_single_list = SingleList.new();

func clone() -> SudokuGrid:
	# Deep clone of house_candidates
	var house_candidates: Array = [];
	for entry in self.house_candidates:
		house_candidates.push_back(entry.duplicate());

	# Share existing structure and house_candidates clone
	var new_grid: SudokuGrid = get_script().new(cells.duplicate(), houses, houses_per_cell, neighbours, house_candidates) as SudokuGrid;

	new_grid.hidden_single_list = hidden_single_list.clone();
	new_grid.naked_single_list = naked_single_list.clone();
	new_grid.solved_cell_count = solved_cell_count;
	
	return new_grid;

############
# Structure creation methods
############
func create_cells() -> Array:
	var cells: Array = [];

	for i in range(GRID_SIZE * GRID_SIZE):
		cells.push_back(Cell.new_cell());
	
	return cells;

func create_houses() -> Array:
	var houses: Array = [];
	for row in create_rows():
		houses.push_back(row);
	for col in create_cols():
		houses.push_back(col);
	for box in create_boxes():
		houses.push_back(box);
	return houses;

func create_houses_per_cell() -> Array:
	var houses_per_cell: Array = [];
	for i in range(cells.size()):
		houses_per_cell.push_back([]);
	
	for i in range(houses.size()):
		var house: Array = houses[i];
		for cell_index in house:
			houses_per_cell[cell_index].push_back(i);

	return houses_per_cell;

func create_neighbours() -> Array:
	var neighbours: Array = [];
	for cell_index in range(cells.size()):
		neighbours.push_back([]);
		for house_index in houses_containing_cell(cell_index):
			for neighbour_index in houses[house_index]:
				if !neighbours[cell_index].has(neighbour_index) and cell_index != neighbour_index:
					neighbours[cell_index].push_back(neighbour_index);
	return neighbours;

func create_house_candidates() -> Array:
	var house_candidates: Array = [];
	for house in range(houses.size()):
		var candidates: Array = [];
		for candidate in range(GRID_SIZE):
			candidates.push_back(GRID_SIZE);
		house_candidates.push_back(candidates);
	return house_candidates;

func create_rows() -> Array:
	return create_houses_from_indices("get_row_indices");

func create_cols() -> Array:
	return create_houses_from_indices("get_col_indices");

func create_boxes() -> Array:
	return create_houses_from_indices("get_box_indices");

func create_houses_from_indices(index_function) -> Array:
	var houses: Array = [];
	for i in range(GRID_SIZE):
		var house: Array = [];
		var indices: Array = self.call(index_function, i);
		for j in range(GRID_SIZE):
			house.push_back(indices[j]);
		houses.push_back(house);

	return houses;

############
# Structure queries
############
func get_row_indices(row_number: int) -> Array:
	return range(GRID_SIZE * row_number, GRID_SIZE * (row_number + 1));

func get_col_indices(col_number: int) -> Array:
	return range(col_number, GRID_SIZE * GRID_SIZE, GRID_SIZE);

func get_box_indices(box_number: int) -> Array:
	var indices: Array = [];
	for i in range(GRID_SIZE):
		indices.push_back(BOX_CORNERS[box_number] + BOX_OFFSETS[i]);
	return indices;

func houses_containing_cell(index: int) -> Array:
	return houses_per_cell[index];

func cell_neighbours(index: int) -> Array:
	return neighbours[index];

func cell_can_see_cell(index_a: int, index_b: int) -> bool:
	for house_index in houses_containing_cell(index_a):
		if houses_containing_cell(index_b).has(house_index):
			return true;
	return false;

func houses_containing_cells(cells: Array) -> Array:
	var houses: Array = [];
	for house in houses_containing_cell(cells[0]):
		var all_cells = true;
		for index in cells:
			all_cells = all_cells and houses_containing_cell(index).has(house);
		
		if all_cells:
			houses.push_back(house);

	return houses;

############
# State queries
############
func is_valid() -> bool:
	return valid;
	
func solved_count() -> int:
	return solved_cell_count;

func unsolved_count() -> int:
	return cells.size() - solved_count();

func get_solved_count_per_value(value: int) -> int:
	var count = 0;
	for cell in cells:
		if Cell.is_filled(cell) and Cell.get_value(cell) == value:
			count += 1;
	return count;

func get_single() -> Array:
	if naked_single_list.has_single():
		return naked_single_list.get_single();
	if hidden_single_list.has_single():
		return hidden_single_list.get_single();
	return [];

func cell_can_contain(index: int, candidate: int) -> bool:
	for neighbour in neighbours[index]:
		if Cell.get_value(cells[neighbour]) == candidate:
			return false;
	return true;

func candidates_in_cell_union(indices: Array) -> int:
	var union: int = 0;
	for index in indices:
		union = union | cells[index];
	return union;

func cell_index_union(indices: Array) -> int:
	var union: int = 0;
	for index in indices:
		union = union | Cell.CANDIDATE_MASKS[index + 1];
	return union;
	

############
# Output
############
func as_string() -> String:
	var string: String = "";
	for cell in cells:
		if Cell.is_given(cell):
			string += String(Cell.get_value(cell));
			string += "!";
		elif Cell.is_set(cell):
			string += String(Cell.get_value(cell));
			string += "+";
		else:
			for candidate in Cell.get_candidates(cell):
				string += String(candidate);
			string += "?";
		
	return string;
	
func as_simple_string() -> String:
	var string: String = "";
	for cell in cells:
		if Cell.is_filled(cell):
			string += String(Cell.get_value(cell));
		else:
			string += ".";
		
	return string;

############
# Read-only modifiers
############
func set_cell_value(index: int, value: int) -> SudokuGrid:
	var new_grid: SudokuGrid = self.clone();
	return new_grid.set_cell_value_inplace(index, value);

func unset_cell_value(index: int) -> SudokuGrid:
	var new_grid: SudokuGrid = self.clone();
	return new_grid.unset_cell_value_inplace(index);

func add_candidate(index: int, value: int) -> SudokuGrid:
	var new_grid: SudokuGrid = self.clone();
	return new_grid.add_candidate_inplace(index, value);

func remove_candidate(index: int, value: int) -> SudokuGrid:
	var new_grid: SudokuGrid = self.clone();
	return new_grid.remove_candidate_inplace(index, value);

#############
# In-place modifiers
#############
func set_cell_value_inplace(index: int, value: int, state = Cell.State.SET, safe: bool = true) -> SudokuGrid:
	if Cell.is_filled(cells[index]):
		return self;
	
	cells[index] = Cell.set_value(cells[index], value, state);
	solved_cell_count += 1;
	
	for neighbour in neighbours[index]:
		# Decrement house candidate counters for the removal of candidates in the neighbouring cells
		remove_candidate_inplace(neighbour, value, safe);

		# Check that value isn't present in neighbours
		if Cell.get_value(cells[neighbour]) == value:
			valid = false;
			
	# Decrement house candidate counters for the removal of all candidates in this cell
	for candidate in Cell.get_all_candidates():
		remove_candidate_inplace(index, candidate, safe);

	return self;

func unset_cell_value_inplace(index: int) -> SudokuGrid:
	if !Cell.is_filled(cells[index]):
		return self;

	var value: int = Cell.get_value(cells[index]);
	cells[index] = Cell.unset_value(cells[index]);
	solved_cell_count -= 1;

	for candidate in Cell.get_all_candidates():
		if cell_can_contain(index, candidate):
			add_candidate_inplace_unsafe(index, candidate);
	
	for neighbour in neighbours[index]:
		if !Cell.is_filled(cells[neighbour]) and cell_can_contain(neighbour, value):
			add_candidate_inplace_unsafe(neighbour, value);

	recalculate();

	return self;

func add_candidate_inplace(index: int, value: int) -> SudokuGrid:
	add_candidate_inplace_unsafe(index, value);
	recalculate();
	return self;

func add_candidate_inplace_unsafe(index: int, value: int) -> SudokuGrid:
	if Cell.has_candidate(cells[index], value):
		return self;
	
	cells[index] = Cell.add_candidate(cells[index], value);
	
	if Cell.candidate_count(cells[index]) == 1:
		add_naked_single(index);
	elif Cell.candidate_count(cells[index]) == 2:
		remove_naked_single(index);
	
	for house_index in houses_containing_cell(index):
		var new_count: int = house_candidates[house_index][value - 1] + 1;
		house_candidates[house_index][value - 1] = new_count;
		if new_count == 1:
			add_hidden_single(house_index, value);
		elif new_count == 2:
			remove_hidden_single(house_index, value);
			
	return self;

func remove_candidate_inplace(index: int, value: int, safe: bool = true) -> SudokuGrid:
	if Cell.has_candidate(cells[index], value):
		cells[index] = Cell.remove_candidate(cells[index], value);

		if !safe:
			return self;

		if Cell.candidate_count(cells[index]) == 1:
			add_naked_single(index);
		elif Cell.candidate_count(cells[index]) == 0 and !Cell.is_filled(cells[index]):
			valid = false;
		
		for house_index in houses_containing_cell(index):
			var new_count: int = house_candidates[house_index][value - 1] - 1;
			house_candidates[house_index][value - 1] = new_count;
			if new_count == 1:
				add_hidden_single(house_index, value);
			elif new_count == 0:
				remove_hidden_single(house_index, value);

	return self;

func add_hidden_single(house_index: int, value: int) -> void:
	for cell_index in houses[house_index]:
		if Cell.has_candidate(cells[cell_index], value):
			hidden_single_list.add_single(cell_index, value);

func remove_hidden_single(house_index: int, value: int) -> void:
	for single in hidden_single_list.singles:
		if houses_containing_cell(single[0]).has(house_index) and value == single[1]:
			hidden_single_list.remove_single(single[0]);
			return;

func add_naked_single(cell_index: int) -> void:
	var value: int = Cell.get_first_candidate(cells[cell_index]);
	naked_single_list.add_single(cell_index, value);

func remove_naked_single(cell_index: int) -> void:
	naked_single_list.remove_single(cell_index);

############
# Structure recalculation
############
func recalculate() -> void:
	recalculate_house_candidates();
	recalculate_hidden_singles();
	recalculate_naked_singles();
	recalculate_solved_cell_count();

func recalculate_house_candidates() -> void:
	for house_index in range(houses.size()):
		house_candidates[house_index] = [0,0,0,0,0,0,0,0,0];
		for cell_index in houses[house_index]:
			for candidate in Cell.get_all_candidates():
				if Cell.has_candidate(cells[cell_index], candidate):
					house_candidates[house_index][candidate - 1] += 1;

func recalculate_hidden_singles() -> void:
	self.hidden_single_list = SingleList.new();

	for house_index in range(houses.size()):
		for candidate in Cell.get_all_candidates():
			if house_candidates[house_index][candidate - 1] == 1:
				add_hidden_single(house_index, candidate);

func recalculate_naked_singles() -> void:
	self.naked_single_list = SingleList.new();

	for cell_index in range(cells.size()):
		if Cell.candidate_count(cells[cell_index]) == 1:
			add_naked_single(cell_index);

func recalculate_solved_cell_count() -> void:
	self.solved_cell_count = 0;
	for cell in cells:
		if Cell.is_filled(cell):
			self.solved_cell_count += 1;
