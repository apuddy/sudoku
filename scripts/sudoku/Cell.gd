extends Reference

class_name Cell

# A cell is a bit field in a 32 bit integer. 
# This class modifies those integers to manipulate cell information.
# Lower 9 bits indicate the candidates. eg, ....000001011 represents [1,2,4]
# Bits 10 to 12 represent state. Bit 10 means unfilled, bit 11 means set, bit 12 means given.
# Bits 13 to 16 represent value. 4 bits to represent 0 to 9.

enum State {
	UNFILLED = 0, 
	SET = 1, 
	GIVEN = 2
};

const CANDIDATE_MASKS = [
	0x000, # 0
	0x001, # 1, 000000001
	0x002, # 2, 000000010
	0x004, # 3, 000000100
	0x008, # 4, 000001000
	0x010, # 5, 000010000
	0x020, # 6, 000100000
	0x040, # 7, 001000000
	0x080, # 8, 010000000
	0x100, # 9, 100000000
];

const STATE_MASKS = [
	0x200, # Unfilled
	0x400, # Set
	0x800, # Given
];

# Pre-computed lookup table to map candidate bitmask to candidate count
const CANDIDATE_COUNTS = [
	0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 
	1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
	1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
	1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
	3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
	1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
	3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
	3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
	3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
	4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8, 
	1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
	3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
	3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
	3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
	4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8, 
	2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
	3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
	3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
	4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8, 
	3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
	4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8, 
	4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8, 
	5, 6, 6, 7, 6, 7, 7, 8, 6, 7, 7, 8, 7, 8, 8, 9];

const VALUE_MASK = 0xF000;
const VALUE_SHIFT = 12;
const CLEAR_STATE_MASK = 0xFFFFF1FF;
const ALL_CANDIDATE_MASK = 0x1FF;
const DEFAULT_MASK = 0x901FF;

static func new_cell() -> int:
	return DEFAULT_MASK;

static func is_given(cell: int) -> bool:
	return cell & STATE_MASKS[State.GIVEN];

static func is_set(cell: int) -> bool:
	return cell & STATE_MASKS[State.SET];

static func is_filled(cell: int) -> bool:
	return is_given(cell) or is_set(cell);

static func set_state(cell: int, state: int) -> int:
	cell = cell & CLEAR_STATE_MASK;
	return cell | STATE_MASKS[state];

static func has_candidate(cell: int, candidate: int) -> bool:
	if candidate < 0 or candidate > 9:
		return false;
	return cell & CANDIDATE_MASKS[candidate];

static func has_only_candidates_mask(cell: int, candidates: int) -> bool:
	return (cell ^ candidates) & ALL_CANDIDATE_MASK == 0;

static func remove_candidate(cell: int, candidate: int) -> int:
	if has_candidate(cell, candidate):
		cell = cell & ~CANDIDATE_MASKS[candidate];
	return cell;

static func add_candidate(cell: int, candidate: int) -> int:
	if !has_candidate(cell, candidate):
		cell = cell | CANDIDATE_MASKS[candidate];
	return cell;

static func set_candidates(cell: int, candidates: Array) -> int:
	cell = cell & ~ALL_CANDIDATE_MASK;
	for candidate in candidates:
		cell = add_candidate(cell, candidate);
	return cell;

static func reset_candidates(cell: int) -> int:
	return cell | ALL_CANDIDATE_MASK;

static func get_value(cell: int) -> int:
	return (cell & VALUE_MASK) >> VALUE_SHIFT;

static func set_value(cell: int, value: int, state = State.SET) -> int:
	cell = cell & ~VALUE_MASK;
	cell = cell | (value << VALUE_SHIFT);
	return set_state(cell, state);

static func unset_value(cell: int) -> int:
	return set_value(cell, 0, State.UNFILLED);

static func candidate_count(cell: int) -> int:
	return CANDIDATE_COUNTS[cell & ALL_CANDIDATE_MASK];

static func get_all_candidates() -> Array:
	return [1, 2, 3, 4, 5, 6, 7, 8, 9];

static func get_candidates(cell: int) -> Array:
	var array: Array = [];

	for i in range(1, CANDIDATE_MASKS.size()):
		var candidate: int = cell & CANDIDATE_MASKS[i];
		if candidate != 0:
			array.push_back(i);

	return array;

static func get_first_candidate(cell: int) -> int:
	for i in range(1, CANDIDATE_MASKS.size()):
		var candidate: int = cell & CANDIDATE_MASKS[i];
		if candidate != 0:
			return i;

	return -1;

